1.3.7
    * enable 3rd tracking camera for VIO on starling 2
    * disable ground occlusion on downward cameras
1.3.6
    * bump version to fix build
1.3.5
    * fix D0014 Starling 2 downward tracking extrinsics
    * add D0009 extrisnics
1.3.4
    * add voxl-clear-logs command
1.3.3
    * set tracking_left_right to use normalized image
1.3.2
    * add lepton to D0013 extrinsics and hires to D0012/D0014
    * update voxl-inspect-extrinsics
1.3.1
    * add function vcc_find_extrinsic()
    * add lepton extrinsics to D0013
    * add hires extrinsics to D0012 D0014
    * voxl-inspect-extrinsics can now print individual relations
1.3.0
    * new vio-cam config utilities and functions in voxl_common_config
1.2.4
    * removed esc pwm and tone tools, they moved to voxl-io-server
1.2.3
    * starling 2 max hw rev 2 extrinsics
    * add name print to voxl-inspect-extrinsics
1.2.2
    * fix d0008_v5 extrinsics
1.2.1
    * add voxl-inspect-vio tool
1.2.0
    * add d0008_v5 extrinsics
1.1.9
    * add voxl-send-esc-pwm-cmd tool
1.1.8
    * add D0015 extrinsics
1.1.7
    * add option to print a single measurement and return in voxl-inspect-vibration
    * voxl-inspect vibration now defaults to imu_apps for ease of use
    * new extrinsics for D0012 Starling 2 Max with C29 config (with TOF)
1.1.6
    * new extrinsics for d0012 and d0014
1.1.5
    * update to use new tof2_data_t type
    * add more data to voxl-inspect-tof
1.1.4
    * update FPV tracking extrinsics for new mount
    * add a downward tracking extrinsic for experimentation
1.1.3
    * fix erroneos "fail" print during configure extrinsics
    * fix y location not being printed in voxl-inspect-detections
    * add D0011 starling px4 edition extrinsics
1.1.2
    * add D0013 extrinsics file
    * update fpv revb extrinsics file for tracking cam mount revision
    * improve voxl-inspect-detections
1.1.1
    * add helpful instructions to view rgb output in voxl-record-raw-image
1.1.0
    * new tool voxl-record-raw-image
1.0.5
    * csv format option in voxl-inspect-imu
    * cleanup unused fields in Sentinel and Starling extrinsics files
1.0.4
    * voxl-camera-server reports when camera server disconnects
1.0.3
    * voxl-camera-server print h264/h265 i vs p frame
    * voxl-record-video supports h265 now
1.0.2
    * add mbps to voxl-inspect-cam
    * fix fps meter
    * fix tester
1.0.1
    * set up tracking extrinsics for FPV experimentally
1.0.0
    * move voxl-configure-mpa out into voxl-configurator
0.10.2
    * fix inspect battery and inspect gps pipe name for mavlink server overhaul
    * new tool voxl-inspect-mavlink
0.10.1
    * add voxl-configure-px4-params for D0005 and D0008
0.10.0
    * new tool voxl-check-calibration
    * enable qvio on fpv
0.9.5
    * add voxl-modem
0.9.4
    * turn on streamer for starling, sentinel, and rb5
    * add non-interactive mode to voxl-configure-mpa
0.9.3
    * fix lower case parsing for fpv in voxl-configure-extrinsics
0.9.2
    * remove package dependencies to stop dockers from installing extra stuff at build time
0.9.1
    * voxl-inspect-cam filters the latency for better usability
0.9.0
    * configure-mpa cleanup
    * extrinsics for starling and fpv
    * integration with camera config
    * configure-mpa wizard
0.8.8
    * update fpv and starling extrinsics for lepton, not final yet!!
0.8.7
    * add voxl-configure-px4 to voxl-configure-mpa
0.8.6
    * point some mavlink inspect tools to new pipe locations
    * voxl-configure-mpa: add voxl-uvc-server, update fpv config
0.8.5
    * migration from voxl-vision-px4 to voxl-vision-hub
0.8.4
    * add flow and tracker to voxl-configure-mpa
    * add a starting point fpv extrinsics file (not perfect)
0.8.3
    * lepton setup
    * fix cam config bug in voxl-configure-mpa
    * remove old -fc option from voxl-configure-mpa
0.8.2
    * voxl-configure-mpa family MRB-D0008
    * cleanup voxl-configure-mpa family setups
0.8.1
    * add inspect cam ascii tool for viewing camera images in terminal
0.8.0
    * add really basic voxl-record-video POC
0.7.6
    * voxl-configure-mpa now assumes hires+tof+tracking (6) for starling v2
0.7.5
    * Retag to force rebuild against new libmodal-pipe pointcloud format
0.7.4
    * update starling v2 extrinsics
0.7.3
    * more sigfigs for gyro in voxl-inspect-vibration
    * add libmodal-pipe debug mode option to voxl-inspect-imu for pipe debugging
0.7.2
    * Fix configure mpa enabling wrong imu server on rb5
0.7.1
    * new tool voxl-inspect-detections
0.7.0
    * new function vcc_find_extrinsic()
    * new stereo extrinsics for starling V2
0.6.5
    * improve help text in voxl-inspect-imu
0.6.4
    * get starling v2 working in configure-mpa and extrinsics
0.6.3
    * Sentinel and starling extrinsic updates
    * remove old default values from extrinsic file parser
0.6.2
    * Move voxl-inspect-services to voxl-utils
0.6.1
    * voxl-configure-mpa set qvio to use imu0 on qrb5165
0.6.0
    * split out voxl-logger
0.5.1
    * New CI Stuff
0.5.0
    * new tool voxl-inspect-tof
    * voxl-logger and replay support for pmd tof data
    * voxl-logger and replay support for hires
0.4.6
    * have voxl-inspect-points print the first point
0.4.5
    * improve voxl-configure-mpa help text and remove wizard
    * add mode for list pipes to only print a specific type
    * add multi-cam support to inspect cam and mode for all cameras
0.4.4
    * add new extrinsics and part numbers to configure-mpa
0.4.3
    * voxl-inspect-services compatability with newer bash version
    * add new extrinsics file and config
0.4.2
    * add 32-bit libs to qrb5165 deb
0.4.0
    * migrate to ipk/deb template
0.3.8
    * add voxl-mavlink-server to configure-mpa script
0.3.7
    * add voxl-check-camera-repetition tool for checking for repeat camera frames
0.3.6
    * voxl-inspect-services no longer tries to source a deprecated file
    * clean up color handling in voxl-inspect-services
0.3.5
    * fix configure-mpa tab completion bug
0.3.4
    * fix ipk conficts/replaces with voxl-utils for smooth upgrade
0.3.3
    * make sure all scripts are executable
    * voxl-inspect-battery indicates when a battery isn't configured
0.3.2
    * move scripts folder to bin and clean up make_package
0.3.1
    * add voxl-portal to voxl-configure-mpa
0.3.0
    * add extrinsics for starling
    * update voxl-configure-mpa to new part number system
0.2.10
    * voxl-inspect-services now only optionally prints version numbers
    * add voxl-list-pipes
0.2.9
    * add another decimal place to inspect-imu gyro
    * rename useeker to just seeker
    * remove prompt from the end of production configuration
0.2.8
    * replace voxl-inspect-services with a bash script that does the same thing
    * add another decimal place to inspect-cam exposure
0.2.7
    * add version numbers to inspect-services
0.2.6
    * add writing production mode to factory file
0.2.5
    * remove apriltag stuff
    * add voxl-configure-tag-detector
0.2.4
    * remove mavlink submodule, use voxl-mavlink instead
0.2.3
    * fix newline voxl-inspect-cam
    * let voxl-replay auto-stop existing pipe servers
0.2.2
    * update to libmodal_pipe v2.0
0.2.1
    * update tools to use libmodal_pipe 2.0
0.2.0
    * add bash completion scripts to inspect tools
0.1.9
    * voxl-inspect-services now greps top for cpu usage
0.1.8
    * add uSeeker extrinsics
0.1.7
    * add voxl-inspect-gps
    * add mavlink submodule
    * add voxl-inspect-battery
    * build for x86_64 too
0.1.6
    * voxl-inspect-points grabs middle of tof frame
0.1.5
    * update inspect tools with auto-reconnect feature
    * --pipe argument is gone for cleaner UX
    * voxl-logger supports float32 images
0.1.4
    * voxl-logger odometry preset includes qvio now
    * voxl-logger improve debug output
0.1.3
    * fix replay of vio data
    * fix imu newline prints when printing all data

0.1.2
    * new mapper extrinsic config
0.1.1
    * build with new opencv
    * change voxl-logger arg for imu+tracking to "preset_odometry"
0.1.0
    * new voxl-inspect-vibration tool
    * new voxl-configure-mpa tool
    * cleanup other inspect tools
    * make adding default extrinsic entries easier
    * add default extrinsic from body to tof
0.0.4
    * add test mode to voxl-inspect-camera
    * update to use libmodal_pipe 1.7.0
0.0.2
    * add voxl-inspect-pose
    * add voxl-configure-extrinsics.sh script
0.0.1
    * initial commit
