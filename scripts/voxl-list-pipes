#!/bin/bash

BASE_DIR="/run/mpa/"

__print_usage() {
	echo ""
	echo "voxl-list-pipes"
	echo ""
	echo "List all of the available MPA pipes"
	echo ""
	echo "Options:"
	echo -e "\t-h|--help        : Show this help message"
    echo -e "\t-t|--mode-types  : Sort by types"
	echo -e "\t-t <type>        : Show only pipes of this type"
	echo ""
}

__contains_word () {
        local w word=$1; shift
        for w in "$@"; do
                [[ $w = "$word" ]] && return 0
        done
        return -1
}

__mode_types() {

	TYPES=($(cat ${BASE_DIR}*/info | grep type | sed s/\"type\"://g | grep -o '".*"' | tr -d '"' | sort | uniq))

	for TYPE in "${TYPES[@]}"; do

		echo "${TYPE}"
		for file in ${BASE_DIR}* ; do
    		if [ -f "${file}/info" ] && cat "${file}/info" | grep -q "\"${TYPE}\"" ; then
    			echo -e "\t$(basename $file)"
    		fi
    	done
    	echo ""

	done

}

__list_pipes_main() {

    MODE=0

    for (( i=1; i <= "$#"; i++ )); do

        PREV_NUM=$(($i-1))
        ARG=${!i}
        PREV_ARG=${!PREV_NUM}

        #echo "arg position: ${i}"
        #echo "arg value: ${!i}"

        case $ARG in

            --mode-types|-t)
                MODE=1
                ;;

            --help|-h)
                __print_usage
                exit 0
                ;;

            *)
                case $PREV_ARG in

                    --mode-types|-t)
                        MODE="$ARG"
                        ;;

                    *)
                        echo "Unknown Option: $ARG"
                        __print_usage
                        exit -1
                esac

        esac
    done

    case $MODE in

        0)
        	# Default behavior
        	# Effectively the same as ls but formats it line by line
        	for file in ${BASE_DIR}* ; do
        		echo -e "$(basename $file)"
        	done
            ;;
        1)
            #Sort by types Mode
            __mode_types
            ;;
        *)
            #Show only specific type
            for file in ${BASE_DIR}* ; do
                if [ -f "${file}/info" ] && cat "${file}/info" | grep -q "\"${MODE}\"" ; then
                    echo -e "$(basename $file)"
                fi
            done
            ;;
    esac
}

__list_pipes_main $@
