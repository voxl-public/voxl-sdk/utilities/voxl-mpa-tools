/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <voxl_common_config.h>

int enabled_only = 0;

static void _print_usage(void)
{
	printf("\n\
Tool to print vio-cam config as loaded from disk.\n\
This is the data consumed by voxl-qvio-server, voxl-feature-tracker, and\n\
voxl-open-vins-server to see what camera and imus to use\n\
\n\
See also: voxl-inspect-apriltag-config\n\
\n\
-h, --help                  print this help message\n\
-e, --enabled_only          only print enabled cameras\n\
\n\
typical usage:\n\
/# voxl-inspect-vio-cams\n\
\n");
	return;
}



static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{"enabled_only",	no_argument,		0,	'e'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "he", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'h':
			_print_usage();
			exit(0);

		case 'e':
			enabled_only = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


int main(int argc, char* argv[])
{
	vio_cam_t cams[VCC_MAX_VIO_CAMS];

	if(parse_opts(argc, argv)) return -1;

	// read in the data
	int n_cams = vcc_read_vio_cam_conf_file(cams, VCC_MAX_VIO_CAMS, enabled_only);
	if(n_cams < 1) return -1;

	// print the values with our helper
	vcc_print_vio_cam_conf(cams, n_cams);

	// also fetch the imu to body relation which openvins needs
	double R_imu_to_body[3][3];
	if(vcc_fetch_R_child_to_body(cams[0].imu, R_imu_to_body)==0){
		int j;
		printf("\nR_%s_to_body:", cams[0].imu);
		printf("\n        ");
		for(j=0;j<3;j++) printf("%7.3f ", R_imu_to_body[0][j]);
		printf("\n        ");
		for(j=0;j<3;j++) printf("%7.3f ", R_imu_to_body[1][j]);
		printf("\n        ");
		for(j=0;j<3;j++) printf("%7.3f ", R_imu_to_body[2][j]);
		printf("\n");
	}



	return 0;
}
