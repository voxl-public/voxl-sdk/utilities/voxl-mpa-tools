/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <stdlib.h>
#include <time.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>


#include "common.h"

// copied from voxl-vision-hub.h to avoid circular dependency since vvhub
// is dependent on voxl-mpa-tools
#define MSP_RECOMMENDED_PIPE_SIZE (4*1024)
#define MSP_OSD_PIPE_PATH_NAME	(MODAL_PIPE_DEFAULT_BASE_DIR "msp_osd/")
#define CLIENT_NAME			"voxl-inspect-osd"

static int test = 0;
static int watchdog = 0;

static void _print_usage(void)
{
	printf("\n\
Tool to print MSP OSD information from msp_osd pipe\n\
\n\
-h, --help              print this help message\n\
-t, --test              exit and print PASS on first msp_osd message\n\
\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{"test",			no_argument,		0,	't'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "ht", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
		case 't':
			test = 1;
			break;
		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}



// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	if(!test){
		printf(CLEAR_TERMINAL DISABLE_WRAP FONT_BOLD);
		printf(" Server connected --> \n");
		printf(RESET_FONT);
	}
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	if(!test){
		fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	}
	return;
}


// callback for simple helper when data is ready
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	if(test && main_running){
		printf("PASS\n");
		main_running = 0;
	}
	else if (!test){
		struct timespec ts;
		clock_gettime(CLOCK_REALTIME, &ts); // Get the current time
		long milliseconds = ts.tv_sec * 1000 + ts.tv_nsec / 1000000;

		printf("<< RX msp_osd [Time] %ld - [Bytes] %d\n", milliseconds, bytes);

		if(data == NULL){
			printf("<< RX msp_osd [Time] %ld - ERROR\n", milliseconds);
		} else {
			printf("<< RX msp_osd [Time] %ld - [Bytes] %d\n", milliseconds, bytes);
		}
		fflush(stdout);
	}
	return;
}


int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// normal non-test mode
	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	if(!test){
		printf(CLEAR_TERMINAL "waiting for MSP Data from OSD pipe: %s\n", MSP_OSD_PIPE_PATH_NAME);
	}
	int ret = pipe_client_open(0, MSP_OSD_PIPE_PATH_NAME, CLIENT_NAME, \
				EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
								MSP_RECOMMENDED_PIPE_SIZE);
	if(ret){
		fprintf(stderr, "ERROR: failed to open pipe %s\n", MSP_OSD_PIPE_PATH_NAME);
		pipe_print_error(ret);
		fprintf(stderr, "Probably voxl-osd is not running\n");
		return -1;
	}

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running){
		usleep(500000); // 0.5sec
		if(test){
			if(watchdog++ > 10){
				main_running = 0;
				printf("FAIL\n");
			}
		}
	}

	// all done, signal pipe read threads to stop
	if(!test){
		printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	}
	pipe_client_close_all();

	return 0;
}
