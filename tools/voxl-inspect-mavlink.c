/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <pthread.h>

#include <c_library_v2/common/mavlink.h>
#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"
#include "mavlink_id_to_string.h"

#define CLIENT_NAME "voxl-inspect-mavlink"
#define MAX_MESSAGE_TYPES 64


static char pipe_path[MODAL_PIPE_MAX_PATH_LEN]="";
static pthread_mutex_t mtx;
static int64_t start_time_ns;
static double elapsed_time_s;


typedef struct mavlink_counter_t{
	int id;
	int counter;
	char mavlink_name[MAX_MAVLINK_NAME_LEN];
}mavlink_counter_t;

static mavlink_counter_t array[MAX_MESSAGE_TYPES];
static int arr_size = 0;

static void _print_usage(void)
{
	printf("\n\
Tool to print Mavlink Packet data from mavlink pipes to the screen for inspection.\n\
\n\
-h, --help              print this help message\n\
\n\
Usage and examples:\n\
voxl-inspect-mavlink {pipe name}\n\
voxl-inspect-mavlink mavlink_onboard\n\
voxl-inspect-mavlink mavlink_to_gcs\n\
voxl-inspect-mavlink mavlink_sys_status\n\
\n\
\n");
	return;
}

static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "n", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
		default:
			_print_usage();
			return -1;
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: There must be one input pipe argument given\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}

// called whenever we disconnect from the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	start_time_ns = _time_monotonic_ns();
	return;
}

static void _print_table(void)
{
	// waiting for data to arrive
	if(array[0].counter==0) return;

	printf(GOTO_TOP_LEFT DISABLE_WRAP FONT_BOLD);
	printf("|   ID  |");
	printf("      Mavlink MSG Name       |");
	printf("Counter|");
	printf("  Hz  |\n");
	printf("|-------|-----------------------------|-------|------|\n");
	printf(RESET_FONT);

	// lock before we start reading the data
	pthread_mutex_lock(&mtx);

	for(int i=0; i<arr_size; i++){
		double hz = (double)array[i].counter / elapsed_time_s;
		printf("|%6d |", array[i].id);
		printf(" %-28s|", array[i].mavlink_name);
		printf("%6d |", array[i].counter);
		printf("%5.1f |", hz);
		printf("\n");
	}
	// only flush once at the end
	fflush(stdout);

	pthread_mutex_unlock(&mtx);

	return;
}


static void addValueToArray(int id)
{
	for(int i = 0; i<arr_size; i++){
		if(array[i].id == id){
			array[i].counter++;
			return; // value already exists in array
		}
	}

	if(arr_size>=MAX_MESSAGE_TYPES){
		fprintf(stderr, "ERROR reached MAX_MESSAGE_TYPES\n");
		return;
	}

	int new_idx=0; // index for the new msg id

	// search for the right location, starting at the end, making space as we go
	for(int i=arr_size-1; i>=0; i--){
		if(id<array[i].id) array[i+1]=array[i];
		else {
			new_idx=i+1;
			break;
		}
	}

	array[new_idx].id = id;
	array[new_idx].counter = 1;
	mavlink_id_to_string(id, array[new_idx].mavlink_name);
	arr_size++;
	return; // value added successfully
}


// callback for simple helper when data is ready
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets, i;
	mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(msg_array == NULL){
		printf("Msg array is null");
		return;
	}

	// log each message
	pthread_mutex_lock(&mtx);

	elapsed_time_s = (double)(_time_monotonic_ns()- start_time_ns)/1000000000.0;
	for(i=0;i<n_packets;i++){
		addValueToArray(msg_array[i].msgid);
	}

	pthread_mutex_unlock(&mtx);

	return;
}

int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	printf(CLEAR_SCREEN);
	printf("waiting for pipe to connect\n");


	// set up all our MPA callbacks
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);

	pipe_client_open(0, pipe_path, CLIENT_NAME, \
					EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
									MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);

	// keep going until the  signal handler sets the running flag to 0
	while(main_running){
		_print_table();
		usleep(200000);
	}

	// all done, signal pipe read threads to stop
	pipe_client_close_all();
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	return 0;
}
