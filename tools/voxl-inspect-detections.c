/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>

#include "common.h"
#include "ai_detection.h"

#define SET_CURSOR_POSITION "\033[%d;1H"
#define CLIENT_NAME			"voxl-inspect-detections"

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static int en_all = 0;


static void _print_usage(void)
{
	printf("\n\
Tool to print ai detection data to the screen for inspection.\n\
By default this will print all packets received each read with\n\
condensed info. Use the --all option to print some extra\n\
data about the detections.\n\
\n\
-a, --all                   print all data received\n\
-h, --help                  print this help message\n\
\n\
example usage:\n\
/# voxl-inspect-detections tflite_data\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"all",				no_argument,		0,	'a'},
		{"help",			no_argument,		0,	'h'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "ah", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'a':
			en_all = 1;
			break;
		case 'h':
			_print_usage();
			exit(0);
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL GOTO_TOP_LEFT DISABLE_WRAP FONT_BOLD);
	if(en_all){
		printf("|    Class Name    |");
		printf("    Camera    |");
		printf(" Class Confidence |");
		printf(" Detection Confidence |");
		printf(" Frame ID |");
		printf("  Timestamp(ns)  |");
		printf(" Detection Center |\n");	
		printf("|------------------|--------------|------------------|----------------------|----------|-----------------|------------------|\n");
	}
	else{
		printf("|    Class Name    |");
		printf("    Camera    |");
		printf(" Class Confidence |");
		printf(" Detection Confidence |\n");
		printf("|------------------|--------------|------------------|----------------------|\n");
	}
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// called when the simple helper has data for us
static void _helper_cb(__attribute__((unused))int ch, char* data, __attribute__((unused))int bytes, __attribute__((unused)) void* context)
{
	int n_packets = bytes / sizeof(ai_detection_t);
	ai_detection_t* detections = (ai_detection_t*) data;
	printf("\033[J");

	for(int i=0;i<n_packets;i++){
		if (detections[i].magic_number != AI_DETECTION_MAGIC_NUMBER) return;
		if (en_all){
			printf("| %-17s|", detections[i].class_name);
			printf("%-14s|", detections[i].cam);
			printf("%17.2f |", (double)detections[i].class_confidence);
			printf("%21.1f |", (double)detections[i].detection_confidence);
			printf("%9d |", detections[i].frame_id);
			printf("%16.1f |", (double)detections[i].timestamp_ns);
			printf("%8.1f %8.1f|", (double) (detections[i].x_min + (detections[i].x_max - detections[i].x_min)/2),(double) (detections[i].y_min + (detections[i].y_max - detections[i].y_min)/2));
		} 
		else {
			printf("| %-17s|", detections[i].class_name);
			printf("%-14s|", detections[i].cam);
			printf("%17.2f |", (double)detections[i].class_confidence);
			printf("%21.1f |", (double)detections[i].detection_confidence);
		}
		printf("\n");
	}
	printf(SET_CURSOR_POSITION, 3);
	fflush(stdout);
	return;
}



int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// prints can be quite long, disable terminal wrapping
	printf(DISABLE_WRAP);

	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	int ret = pipe_client_open(0, pipe_path, CLIENT_NAME, \
				EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
				sizeof(ai_detection_t) * 32);

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	pipe_client_close_all();

	return 0;
}
