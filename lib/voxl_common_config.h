/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef VOXL_COMMON_CONFIG_H
#define VOXL_COMMON_CONFIG_H

#include <stdio.h>
#include <string.h>
#include <modal_json.h>


#ifdef __cplusplus
extern "C" {
#endif


/**
 * This is the default and recommended config file path and is used by both
 * voxl-qvio-server, voxl-vins-server, voxl-dfs-server, and voxl-vision-hub.
 * However other projects can elect to have their own independant config files
 * if they wish.
 */
#define VCC_EXTRINSICS_PATH	"/etc/modalai/extrinsics.conf"

/**
 * This is a config file designed to describe the imu and cameras used for VIO
 * It is used by voxl-qvio-server, voxl-feature-tracker, and voxl-open-vins-server
 **/
#define VCC_VIO_CAMS_PATH	"/etc/modalai/vio_cams.conf"

/*
 * Recommended sane but arbitrary limit on number of extrinsics in one config
 * file to allow for static memory allocation. You can ignore this and set your
 * own limit if you want, both functions here allow setting max through
 * arguments.
 */
#define VCC_MAX_EXTRINSICS_IN_CONFIG 32


/**
 * This configuration file serves to describe the static relations (translation
 * and rotation) between sensors and bodies on a drone. Mostly importantly it
 * configures the camera-IMU extrinsic relation for use by VIO. However, the
 * user may expand this file to store many more relations if they wish. By
 * consolidating these relations in one file, multiple processes that need this
 * data can all be configured by this one configuration file. Also, copies of
 * this file may be saved which describe particular drone platforms. The
 * defaults describe the VOXL M500 drone reference platform.
 *
 * The file is constructed as an array of multiple extrinsic entries, each
 * describing the relation from one parent to one child. Nothing stops you from
 * having duplicates but this is not advised.
 *
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in
 * intrinsic XYZ order in units of degrees. This corresponds to the parent
 * rolling about its X axis, followed by pitching about its new Y axis, and
 * finally yawing around its new Z axis to end up aligned with the child
 * coordinate frame.
 *
 * The helper read function will read out and populate the associated data
 * struct in both Tait-Bryan and rotation matrix format so the calling process
 * can use either. Helper functions are provided to convert back and forth
 * between the two rotation formats.
 *
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for
 * ease of use when doing camera-IMU extrinsic relations in the field. This is
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by
 * the rc_math library. However, since the camera Z axis points out the lens, it
 * is helpful for the last step in the rotation sequence to rotate the camera
 * about its lens after first rotating the IMU's coordinate frame to point in
 * the right direction by Roll and Pitch.
 *
 * The following online rotation calculator is useful for experimenting with
 * rotation sequences: https://www.andre-gaschler.com/rotationconverter/
 *
 * The Translation vector should represent the center of the child coordinate
 * frame with respect to the parent coordinate frame in units of meters.
 *
 * The parent and child name strings should not be longer than 63 characters.
 */
typedef struct vcc_extrinsic_t{
	char parent[64];				///< parent frame name, e.g. "imu0"
	char child[64];					///< child name, e.g. "tracking0" (camera)
	double T_child_wrt_parent[3];	///< translation vector, eg location of camera wrt imu frame
	double RPY_parent_to_child[3];	///< intrinsic Tait-Bryan angles in XYZ (Roll Pitch Yaw) sequence in degrees
	double R_child_to_parent[3][3];	///< rotation matrix, eg from imu to camera frame
} vcc_extrinsic_t;



/**
 * @brief      Retrieves a single extrinsic relation from one parent to one
 *             child from the extrinsics config file.
 *
 *             If the inverse relation exists in the file, this function will
 *             invert the relation for you.
 *
 *             For instances where you wish to retrieve multiple relations, you
 *             can use the vcc_read_extrinsic_conf_file() function to return all
 *             relations from the extrinsics config file.
 *
 * @param[in]  parent  The parent frame
 * @param[in]  child   The child frame
 * @param[out] out     pointer to where the result should be written if
 *                     successful
 *
 * @return     0 on success, -1 on failure
 */
int vcc_fetch_extrinsic(const char* parent, const char* child, vcc_extrinsic_t* out);



/**
 * @brief      print out an array of extrinsic configuration structs
 *
 *             you can also point to a single struct and set n=1
 *
 * @param      t     pointer to extrinsic struct array
 * @param[in]  n     number of extrinsic config structs to print
 */
void vcc_print_extrinsic_conf(vcc_extrinsic_t* t, int n);


/**
 * @brief      Reads an extrinsic configuration file.
 *
 * @param[in]  path   file path, usually EXTRINSICS_PATH
 * @param      t      pointer to array of config structs to write out to
 * @param      n      pointer to write out number of tags parsed from the file
 * @param[in]  max_t  The maximum number of structs that array t can hold
 *
 * @return     0 on success, -1 on failure
 */
int vcc_read_extrinsic_conf_file(const char* path, vcc_extrinsic_t* t, int* n, int max_t);



/**
 * @brief      Finds an extrinsic configuration with specified parent and child
 *             in an array of extrinsics.
 *
 *             This is like vcc_find_extrinsic_in_array except it will also
 *             combine two existing transforms if the exact user requested
 *             transform is not explicitly listed but could be calculated with
 *             a single intermediary.
 *
 *             This function is a placeholder until we implement a full graph
 *             data structure to allow calculating between any two coordinate
 *             frames that are linked.
 *
 * @param[in]  parent  The parent frame, e.g. "imu_apps"
 * @param[in]  child   The child frame, e.g. "tracking" camera
 * @param      t       array of extrinsic_t structs to search through
 * @param[in]  n       number of structs in array t
 * @param      out     pointer to user's extrinsic_t struct to write out to
 *
 * @return     0 on success, -1 on failure
 */
int vcc_find_extrinsic(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out);


/**
 * @brief      quick helper to retrieve the rotation matrix from a frame
 *             (usually imu_apps) to body
 *
 *
 *             The imu_apps to body rotation is used by some vio systems to
 *             figure out which orientation the imu is mounted relative to the
 *             drone/gravity. It's the equivalent of reading the whole
 *             extrinsics file and searching for the relation between the "body"
 *             frame (parent) and the specified child frame, usually imu_apps.
 *
 *             See voxl-inspect-vio-cams utility for an example
 *
 * @param[in]  child            The child
 * @param[out] R_child_to_body  The r imu to body
 *
 * @return     0 on success, -1 on failure
 */
int vcc_fetch_R_child_to_body(const char* child, double R_child_to_body[3][3]);


/**
 * @brief      Convert a rotation matrix to a Tait-Bryan intrinsic XYZ (Roll
 *             Pitch Yaw) rotation sequence in degrees
 *
 *             Unlike the rc_math library which consistently uses the more
 *             common aerospace-standard ZYX Tait-Bryan rotation sequence in
 *             radians, this function uses the XYZ Tait-Bryan rotation sequence
 *             in degrees. The reason for this is that a primary use for the
 *             extrinsics config file is setting the IMU-to-Camera extrinsic
 *             relation which is most easily done in the field by observation in
 *             units of degrees instead of radians. Also, since the camera Z
 *             axis points out the lens, it is helpful for the last step in the
 *             rotation sequence to rotate the camera about its lens after
 *             getting rotating it to point in the right direction by Roll and
 *             Pitch.
 *
 *             Note that a rotation sequence that intrinsically rotates a
 *             coordinate frame A to align with coordinate frame B will convert
 *             to a rotation matrix that rotates a vector from being represented
 *             in frame B to frame A.
 *
 * @param[in]  R     Rotation Matrix
 * @param[out] tb    resulting Tait-Bryan intrinsic XYZ (Roll Pitch Yaw)
 *                   rotation sequence in degrees
 */
void vcc_rotation_matrix_to_tait_bryan_xyz_degrees(double R[3][3], double tb[3]);


/**
 * @brief      convert Tait-Bryan intrinsic XYZ (Roll Pitch Yaw) rotation
 *             sequence in degrees to a rotation matrix
 *
 *             Unlike the rc_math library which consistently uses the more
 *             common aerospace-standard ZYX Tait-Bryan rotation sequence in
 *             radians, this function uses the XYZ Tait-Bryan rotation sequence
 *             in degrees. The reason for this is that a primary use for the
 *             extrinsics config file is setting the IMU-to-Camera extrinsic
 *             relation which is most easily done in the field by observation in
 *             units of degrees instead of radians. Also, since the camera Z
 *             axis points out the lens, it is helpful for the last step in the
 *             rotation sequence to rotate the camera about its lens after
 *             getting rotating it to point in the right direction by Roll and
 *             Pitch.
 *
 *             Note that a rotation sequence that intrinsically rotates a
 *             coordinate frame A to align with coordinate frame B will convert
 *             to a rotation matrix that rotates a vector from being represented
 *             in frame B to frame A.
 *
 * @param[in]  tb    tait-bryan intrinsic XYZ (Roll Pitch Yaw) rotation sequence
 * @param[out] R     Resulting rotation matrix
 */
void vcc_tait_bryan_xyz_degrees_to_rotation_matrix(double tb[3], double R[3][3]);


/**
 * @brief      convert Tait-Bryan intrinsic XYZ (Roll Pitch Yaw) rotation
 *             sequence in degrees to a rotation vector.
 *
 *             The rotation vector is similar to the axis-angle or Euler angle
 *             representation where the length of the vector is not 1, but has
 *             the same length as the rotation angle in radians. This function
 *             is specifically for reading from the vcc extrinsics file and
 *             converting the imu to camera rotation to the "ombc" rotation
 *             vector used by the Qualcomm MV library for voxl-qvio-server but
 *             could obviously be used elsewhere if appropriate.
 *
 * @param[in]  tb_deg  The tait bryan xyz rotation sequence RPY_parent_to_child
 *                     found in vcc_extrinsic_t
 * @param[out] rv      array to write the rotation vector out to
 *
 * @return     0 on success, -1 on failure
 */
int vcc_tait_bryan_intrinsic_degrees_to_rotation_vector(double tb_deg[3], float rv[3]);


/**
 * @brief Generic structure for a mono lens
 *
 */
#define VCC_MAX_DISTORTION_COEFS 12
typedef struct vcc_lens_cal_t {
	int width;
	int height;
	float fx;              //< Focal length (x) in pixels
	float fy;              //< Focal length (y) in pixels
	float cx;              //< Optical center (x) in pixels
	float cy;              //< Optical center (y) in pixels
	int n_coeffs;          //< 4 for fisheye, 5 for OpenCV pinhole
	int is_fisheye;        //< 1 for 4-param fisheye
	float D[VCC_MAX_DISTORTION_COEFS]; //< Distortion coefficients
	int reserved_1;
	int reserved_2;
} vcc_lens_cal_t;


/**
 * @brief Populates a lens cal struct based on an intrinsics file
 *
 *
 * @param intrinsics_file The intrinsics file to be read
 * @param lens_params Output lens parameter object
 * @return 0 if successful, -1 otherwise
 */
int vcc_read_lens_cal_file(const char* file, vcc_lens_cal_t* lens_cal, int use_second_cam_if_stereo);


// usually allocate an array of vio_cam_t structs of this length. It is more than
// can be used at once in vio but allows for many cameras to be configured but
// only a few to be enabled for easy swapping and testing.
#define VCC_MAX_VIO_CAMS	12

// struct containing all the information a feature tracker or VIO algorithm needs
// to use a camera including what pipes to subscribe to, what imu to use,
// extrinsics, and intrinsics. See the voxl-configure-vio-cams and
// voxl-inspect-vio-cams tools.
typedef struct vio_cam_t{
	int enable;						///< flag indicating if the cam should be used
	char name[64];					///< basic name, should match extrinsic name
	char pipe_for_preview[64];		///< pipe to view the camera stream, usually the same as the name but not necessarily
	char pipe_for_tracking[64];		///< if not empty, this is the pipe used for image tracking, usually a normalized preprocessed pipe
	int is_occluded_on_ground;		///< set to 1 for downward cameras that should be ignored until airborn
	char imu[64];					///< imu to use for this camera
	int is_extrinsic_present;		///< 1 if extrinsic was found and parsed
	vcc_extrinsic_t extrinsic;		///< extrinsic from imu (parent) to child (cam)
	char cal_file[128];				///< intrinsic lens cal file name
	int is_cal_present;				///< 1 if the cal file was found and parsed
	vcc_lens_cal_t cal;				///< intrinsic lens cal data
} vio_cam_t;


/**
 * @brief      print out an array of vio_cam_t configuration structs
 *
 * @param      t       pointer to vio_cam_t struct array
 * @param[in]  n       number of config structs to print
 */
void vcc_print_vio_cam_conf(vio_cam_t* t, int n);


/**
 * @brief      Reads an extrinsic configuration file.
 *
 * to use, create an array of vio_cam_t structs, usually of length VCC_MAX_VIO_CAMS
 * and pass a pointer to this function. Also provide the number of cams, n, that
 * you wish to read which should be the length of your array. To automatically
 * skip disabled cameras then set the only_read_enabled flag to be non-zero. You
 * should definitely set that flag in voxl-qvio-server voxl-feature-tracker and
 * voxl-open-vins-server to save logic in parsing and to make sure camera indices
 * are consistent when enabling and disabling cameras for testing.
 *
 * If you only want to read the first enabled camera, for example on a mono-cam
 * vio system like voxl-qvio-server, then set n to 1 and only_read_enabled to 1
 * and it will simply read out the first enabled camera.
 *
 * @param[out] c   pointer to array of config structs to write out to
 * @param[in]  n   number of items in your allocated array c
 * @param[in]  only_read_enabled
 *
 * @return     -1 on failure, otherwise returns the number of cams read
 */
int vcc_read_vio_cam_conf_file(vio_cam_t* c, int n, int only_read_enbaled);






/**
 * @brief      Finds an extrinsic configuration with specified parent and child
 *             in an array of extrinsics.
 *
 *             This is used to grab the extrinsic configuration that you want
 *             after loading everything from a file. If multiple extrinsic
 *             configurations exist with the desired parent and child then the
 *             first one will be returned.
 *
 *             If a transform is listed in the array with reversed parent and
 *             child then the transform will automatically be inverted.
 *
 * @param[in]  parent  The parent frame, e.g. "imu_apps"
 * @param[in]  child   The child frame, e.g. "tracking" camera
 * @param      t       array of extrinsic_t structs to search through
 * @param[in]  n       number of structs in array t
 * @param      out     pointer to user's extrinsic_t struct to write out to
 *
 * @return     0 on success, -1 on failure
 */
__attribute__((deprecated("\nPlease use vcc_find_extrinsic or vcc_fetch_extrinsic instead")))
int vcc_find_extrinsic_in_array(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out);



#ifdef __cplusplus
}
#endif

#endif // end #define VOXL_COMMON_CONFIG_H
