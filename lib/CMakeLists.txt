cmake_minimum_required(VERSION 3.3)

set(LIBNAME voxl_common_config)
# Build lib from all source files
file(GLOB all_src_files *.c)
add_library(${LIBNAME} SHARED ${all_src_files})

if(CMAKE_SYSTEM_PROCESSOR MATCHES "^aarch64")
	target_link_libraries(${LIBNAME} LINK_PUBLIC /usr/lib64/libmodal_json.so m)
else()
	target_link_libraries(${LIBNAME} LINK_PUBLIC modal_json m)
endif()

# make the include directory public for install
set_target_properties(${LIBNAME} PROPERTIES PUBLIC_HEADER voxl_common_config.h)

# make sure everything is installed where we want
# LIB_INSTALL_DIR comes from the parent cmake file
install(
	TARGETS			${LIBNAME}
	LIBRARY			DESTINATION ${LIB_INSTALL_DIR}
	RUNTIME			DESTINATION /usr/bin
	PUBLIC_HEADER	DESTINATION /usr/include
)
